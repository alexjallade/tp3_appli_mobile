package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.City;
import fr.uavignon.ceri.tp3.data.WeatherRepository;

public class MainViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private WeatherRepository repository;
    private MutableLiveData<City> city;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable>webServiceThrowable;

    public MainViewModel (Application application) {
        super(application);
        repository = WeatherRepository.get(application);
    }

    public void loadWeatherAllcities(){
        repository.loadWeatherAllcities();
    }

}
