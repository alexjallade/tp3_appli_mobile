package fr.uavignon.ceri.tp3.data.webservice;

public class Forecast {
    public final String name;
    public final int temperature;
    public final int humidity;
    public final int wind;
    public final int cloudiness;

    public Forecast(String name, int temperature,
                    String temperatureUnit, String icon, int humidity, int wind, int cloudiness) {
        this.name = name;
        this.temperature = temperature;
        this.humidity = humidity;
        this.wind = wind;
        this.cloudiness = cloudiness;
    }
}
